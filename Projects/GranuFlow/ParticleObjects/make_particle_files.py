def modify_and_save_template(template_filename, line_number, new_content, new_filename):
    # Read the template file
    with open(template_filename, 'r') as file:
        lines = file.readlines()

    # Modify the specified line
    if 1 <= line_number <= len(lines):
        lines[line_number - 1] = new_content + '\n'
    else:
        print(f"Error: Line number {line_number} is out of range.")
        return

    # Append a number to the file name
    filename_parts = template_filename.split('.')

    # Save the modified content to the new file
    with open(new_filename, 'w') as file:
        file.writelines(lines)

    print(f"File saved as {new_filename}")

def save_world_file_text(line, no_particles):
    # Create a new file with the new_filename and write a new line
    log_filename = "world_file_text.txt"
    with open(log_filename, 'a') as log_file:
        log_file.write(f'Index: {line} Name: gd_animation_particles_{line}  Number: {no_particles}\n')

# Loop over diameters
total_number_particles = 35819
all_particle_diameters = [0.00085, 0.00090, 0.00095, 0.001, 0.00105, 0.00110, 0.00115, 0.00120, 0.00125, 0.0013, 0.00135, 0.00140, 0.00145]
psd = [0.00652104, 0.02417671, 0.06181815, 0.11496463, 0.16249064, 0.18105956, 0.16402651, 0.12399944, 0.07997847, 0.04485669, 0.02223769, 0.00988352, 0.00398696]
template_file = "gd_animation_particles_template.POBJ"
line_to_modify = 8  # Replace with the actual line number you want to change

for i, dia in enumerate(all_particle_diameters):
    new_line_content = f"DIAMETER: {dia}"  # Replace with the new content
    new_filename = f"gd_animation_particles_{i}.POBJ"
    modify_and_save_template(template_file, line_to_modify, new_line_content, new_filename)
    no_of_particles_at_diameter = int(psd[i]*total_number_particles)
    save_world_file_text(i, no_of_particles_at_diameter)
