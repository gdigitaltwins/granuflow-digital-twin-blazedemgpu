# GranuFlow Digital Twin - BlazeDEMGPU

[//]: # (Image)

## Description
A discrete element method (DEM) Digital Twin of the Granutools' GranuFlow powder characterisation tool built using BlazeDEM-GPU.
This digital twin allows users to understand in more depth the behaviour of a powder in the GranuFlow. 
Particle properties can be defined and the individual particle data or bulk powder behaviour extracted and analysed.
This digital twin makes use of one of the fastest GPU based open-source DEM software available, BlazeDEM-GPU, to allow for fast simulations of large numbers of particles.

## Example Usage

The GranuFlow digital twin can be used to extract individual particle data for every particle in the system. All results will be output into the 'Results' folder as the simulation runs.

[//]: # (![GranuDrum Example Video]&#40;readme_files/gd_animation_45rpm.mp4&#41;)

[//]: # (### Results)

[//]: # ()
[//]: # (![Graph showing the change in cohesive index with different cohesive energy densities]&#40;readme_files/ci_vs_ced.png&#41;)

[//]: # ()
[//]: # (The graph above shows an example of results that can be obtained from the GranuDrum digital twin. )

[//]: # (Specifically in this case the change in cohesive index with increasing cohesive energy density at different sliding friction values.)

[//]: # (A clear trend showing can be seen showing that as the cohesive energy density increases the cohesive index increases. At the same time )

[//]: # (sliding friction shows very little influence on the cohesive index except at  the highest cohesive energy density.)

## Getting Started
To be able to use this digital twin an Nvidia GPU needs to be available in the machine being used to run the simulation and CUDA needs to be installed.

## How to use the Digital Twin
Using the digital twin is very simple but to explore the behaviour of powder in the GranuFlow digital twin the properties of the powder are required. 
Most of the particle properties can be easily defined by changing the values '.MAT' files in the 'Materials' folder.
The particle size distribution also needs to be defined. This can be done by manually making files in the 'ParticleObjects' folder or by using the 'make_particle_files.py' script.
Once the properties of the powder and particles have been defined the simulation can be started by simply executing the BlazeDEM file in the 'bin' folder.

## Post Processing the Data
The GranuFlow - BlazeDEM digital twin currently outputs VTK files that can be postprocessed in Paraview to visualise the simulation.

[//]: # (This images can then be used with the post-processing script to calculate the dynamic angle of repose and cohesive index.)

[//]: # (To do this set the 'root' variable in the python file 'run_gd_postprocessing.py' to the folder containing the images and run the script.)

[//]: # (The Cohesive Index, Dynamical angle of repose and the 3rd order polynomial fit to the free surface will be saved as a Pandas dataframe in the folder designated in 'df_savepath' variable.)

## Roadmap
- [ ] Include post-processing script to calculate mass flowrate from the BlazeDEM output data.

## Issues
If you run into any issues or bugs let us know by raising and issue on the GitLab! If you find a way to fix it raise a pull request!

## Authors and acknowledgment
The development of GranuTools digital-twins was made during the PhD thesis of Ben Jenkins in the framework 
of a collaboration between GranuTools and University of Birmingham under the academic supervision of 
Dr. Kit Windows-Yule and Prof. Jonathan Seville. Additional thanks to Prof Nicolin Govender (director@blazecomputing.co.uk) 
for questions/support on Blaze and general usage.

## Citing
If you use this project in your publication please cite this Gitlab page and the BlazeDEM paper: https://www.sciencedirect.com/science/article/pii/S235271101630005X


## Project status
Actively being worked on!
